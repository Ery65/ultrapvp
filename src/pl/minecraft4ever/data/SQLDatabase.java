package pl.minecraft4ever.data;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;

import pl.minecraft4ever.glowna.GamePlayer;

public class SQLDatabase implements DatabaseManager {
    Connection con;
    private String fileDir;
    private String fileName;
    private ConsoleCommandSender cons = Bukkit.getServer().getConsoleSender();
    Table tabela;
    
    public SQLDatabase(String fileDir, String fileName) {
        this.fileDir = fileDir;
        this.fileName = fileName;
        
        File dir = new File(fileDir);
        dir.mkdirs();
        
        File file = new File(fileDir + File.separator + fileName);
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        this.open();
    }
    
    protected SQLDatabase() {}
    
    @Override
    public synchronized void setTabela(Table tabela) {
        //check();
        PreparedStatement st = null;
        this.tabela = tabela;
        try {
            st = con.prepareStatement("CREATE TABLE IF NOT EXISTS "+tabela.getNazwa()+tabela.getUzycie());
            st.executeUpdate();
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §cblad podczas towrzenia bazy SQL:");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
            return;
        } finally {
            closeResources(st, null);
        }
    }

    @Override
    public synchronized void set(Object... wartosci) {
        //check();
        PreparedStatement st = null;
        try {
            String wartosci1 = "";
            for(int i = 0; i < wartosci.length; i++) {
                wartosci1 += "?";
                if(i < (wartosci.length - 1))
                    wartosci1 += ",";
            }
            
            st = con.prepareStatement("INSERT INTO "+tabela.getNazwa()+tabela.getWartosci()+" VALUES("+wartosci1+");");
            for(int i = 0; i < wartosci.length; i++) {
                st.setObject(i + 1, wartosci[i]);
            }
            st.executeUpdate();
        } catch (SQLException e) {
            String wartosci1 = "";
            for(int i = 0; i < wartosci.length; i++) {
                wartosci1 += "?";
                if(i < (wartosci.length - 1))
                    wartosci1 += ",";
            }
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §c1 Blad podczas wykonywania zapytania SQL!");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
            cons.sendMessage("INSERT INTO "+tabela.getNazwa()+tabela.getWartosci()+" VALUES("+wartosci1+");");
            return;
        } finally {
            closeResources(st, null);
        }
    }

    @Override
    public synchronized Object get(String index, String toGet, Object wartosci) {
        //check();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("SELECT * FROM "+tabela.getNazwa()+" WHERE "+index+"=?;");
            st.setObject(1, wartosci);
            rs = st.executeQuery();
            if(rs.next()) {
                return rs.getObject(toGet);
            }
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §c2 Blad podczas wykonywania zapytania SQL!");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
        } finally {
            closeResources(st, rs);
        }
        
        return null;
    }

    @Override
    public synchronized boolean contains(String index, Object wartosci) {
        //check();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("SELECT * FROM "+tabela.getNazwa()+" WHERE "+index+"=?;");
            st.setObject(1, wartosci);
            rs = st.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §c3 Blad podczas wykonywania zapytania SQL!");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
            return false;
        } finally {
            closeResources(st, rs);
        }
    }

    @Override
    public synchronized void update(String index, String toUpdate, Object indexValue, Object updateValue) {
        //check();
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("UPDATE "+tabela.getNazwa()+" SET "+toUpdate+"=? WHERE "+index+"=?;");
            st.setObject(1, updateValue);
            st.setObject(2, indexValue);
            st.executeUpdate();
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §c4 Blad podczas wykonywania zapytania SQL!");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
            return;
        } finally {
            closeResources(st, null);
        }
    }

    @Override
    public synchronized void remove(String index, Object wartosci) {
        //check();
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("DELTE FROM "+tabela.getNazwa()+" WHERE "+index+"=?;");
            st.setObject(1, wartosci);
            st.executeUpdate();
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §c5 Blad podczas wykonywania zapytania SQL!");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
            return;
        } finally {
            closeResources(st, null);
        }
    }

    @Override
    public boolean isOpen() {
        return con != null;
    }

    @Override
    public synchronized void open() {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("[UltraPvp] Blad ze sterownikami SQL :");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
            return;
        }
        
        try {
            this.con = DriverManager.getConnection("jdbc:sqlite:" + fileDir + File.separator + fileName);
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §cBlad Podczas laczenia z SQL :");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
            return;
        }
    }

    @Override
    public synchronized void close() {
        try {
            if(con != null)
                con.close();
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §cBlad podczas zamykania polaczenia z SQL");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
        }
    }

    @Override
    public synchronized LinkedHashMap<String, Integer> getxTop(int topLimit) {
        //check();
        PreparedStatement st = null;
        ResultSet rs = null;
        final LinkedHashMap<String, Integer> topPlayers = new LinkedHashMap<String, Integer>(topLimit);
        try {
            st = con.prepareStatement("SELECT * FROM `" + tabela.getNazwa() +"` ORDER BY `punkty` DESC LIMIT " + topLimit);
            rs = st.executeQuery();
            while (rs.next()){
                topPlayers.put(rs.getString("Nazwa"), rs.getInt("punkty"));
            }
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §cBlad podczas tworzenia top 10 :");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
        } finally {
            closeResources(st, rs);
        }
        return topPlayers;
    }
    
    @Override
    public synchronized void resetAllPoints(){
        //check();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("SELECT * FROM `" + tabela.getNazwa() +"` ORDER BY `punkty` DESC");
            rs = st.executeQuery();
            while (rs.next()){
                rs.updateInt("punkty", 0);
                rs.updateInt("zabicia", 0);
                rs.updateInt("smierci", 0);
            }
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §cBlad podczas resetu punktow :");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
        } finally {
            closeResources(st, rs);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public synchronized int getPosition(String player) {
        //check();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("SELECT * FROM `" + tabela.getNazwa() +"` ORDER BY `punkty` DESC");
            rs = st.executeQuery();
            int pos = 1;
            while (rs.next()){
                if (rs.getString("Nazwa").equalsIgnoreCase(player)){
                    return pos;
                } else {
                    pos++;
                }
            }
            return pos;
        } catch (SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §cBlad podczas sprawdzania pozycji gracza :");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
            return 0;
        } finally {
            closeResources(st, rs);
        }
    }
    
    @Override
    public void save(GamePlayer g){
        
    }
    
  //Checking the connection is OK
    public boolean isValidConnection(){
        if (con == null)
            return false;
        try {
            if (con.isValid(1)){
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        try {
            return !con.isClosed();
        } catch (SQLException e) {
            return false;
        }
    }
    
    //Checking the connection and repair it if required
    public synchronized void checkConnection(){
        try {
            if (con != null && !con.isClosed()){
                try {
                    con.isValid(1);
                } catch (SQLException e) {
                    close();
                    open();
                }
            } else
                open();
        } catch (SQLException e){
            open();
        }
    }
    
    //Checking connection
    /*private synchronized void check(){
        if (!isValidConnection()){
            checkConnection();
        }
    }*/
    
    private void closeResources(PreparedStatement st, ResultSet rs){
        if (st != null){
            try {
                st.close();
            } catch (SQLException e) {
                // ignored
            }
        }
        if (rs != null){
            try {
                rs.close();
            } catch (SQLException e) {
                // ignored
            } 
        }
    }

}
