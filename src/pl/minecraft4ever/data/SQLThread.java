package pl.minecraft4ever.data;

public class SQLThread extends Thread {
    
    private boolean running = false;
    private DatabaseManager sql;
    private long delay;
    
    public SQLThread(DatabaseManager sql, long delay) {
        this.sql = sql;
        this.delay = delay;
    }
    
    @Override
    public void run() {
        while(running) {
            try {
                this.sql.close();
                this.sql.open();
            } catch(Throwable t) {
                t.printStackTrace();
            }
            
            try {
                Thread.sleep(this.delay * 1000);
            } catch (InterruptedException e) {
            }
        }
    }
    
    public void start(long delay) {
        this.delay = delay;
        
        if(this.running)
            this.interrupt();
        
        this.start();
    }
    
    @Override
    public void start() {
        if(this.running)
            return;
        
        this.running = true;
        super.start();
    }
    
    @Override
    public void interrupt() {
        if(!this.running)
            return;
        
        this.running = false;
        super.interrupt();
    }
    
    public boolean isRunning() {
        return this.running;
    }

}
