package pl.minecraft4ever.nagrody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;

public class BetterRewards {
    
    private Map<Integer, Double> moneyDifrence = new HashMap<Integer, Double>();
    private Map<Integer, Double> sessionMoney = new HashMap<Integer, Double>();
    
    private Map<Integer, Integer> itemAmountDifrence = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> sessionItems = new HashMap<Integer, Integer>();
    
    private Map<String, Integer> permissionsRank = new HashMap<String, Integer>();
    
    private Map<Integer, List<String>> sessionCommands = new HashMap<Integer, List<String>>();
    
    private boolean works = false;
    
    public Reward getBetterReward(GamePlayer killer, GamePlayer killed){
        Reward r = UltraPvp.getRewardManager().getReward(killer.getMaxNgr(), killer.getLvL(), killed.getName());
        if (!works){
            return r;
        }
        if (UltraPvp.getRewardManager().isSpecialReward(r)){
            return r;
        }
        int krank = permissionsRank.get(killer.getPermission().toLowerCase());
        int drank = permissionsRank.get(killed.getPermission().toLowerCase());
        int dif = drank - krank;
        if (itemAmountDifrence.containsKey(dif)){
            if (r.hasItem()){
                ItemStack it = r.getItem();
                it.setAmount(it.getAmount() + itemAmountDifrence.get(dif));
                r.setItem(it);
            }
            if (r.hasItems()){
                ItemStack[] it = r.getItems();
                int how = itemAmountDifrence.get(dif);
                for (int i = 0; i < it.length; i++){
                    ItemStack its = it[i];
                    its.setAmount(its.getAmount() + how);
                    it[i] = its;
                }
            }
        }
        if (moneyDifrence.containsKey(dif)){
            if (r.hasPrice()){
                r.setPrice(r.getMoney() + moneyDifrence.get(dif));
            }
        }
        int session = killer.getKillsSession();
        if (sessionItems.containsKey(session)){
            if (r.hasItem()){
                ItemStack it = r.getItem();
                it.setAmount(it.getAmount() + sessionItems.get(session));
                r.setItem(it);
            }
            if (r.hasItems()){
                ItemStack[] it = r.getItems();
                int how = sessionItems.get(session);
                for (int i = 0; i < it.length; i++){
                    ItemStack its = it[i];
                    its.setAmount(its.getAmount() + how);
                    it[i] = its;
                }
            }
        }
        if (sessionMoney.containsKey(session)){
            r.setPrice(r.getMoney() + sessionMoney.get(session));
        }
        if (sessionCommands.containsKey(session)){
            if (r.hasCommands()){
                String[] com = r.getCommands();
                String[] com2 = sessionCommands.get(session).toArray(new String[0]);
                String[] com3 = new String[com.length + com2.length];
                for (int i = 0; i < com.length; i++){
                    com3[i] = com[i];
                }
                int i = 0;
                for (int a = com.length; a < com3.length; a++){
                    com3[a] = com2[i];
                    i++;
                }
                r.setCommands(com3);
            } else {
                r.setCommands(sessionCommands.get(session).toArray(new String[0]));
            }
        }
        return r;
    }
    
    public void load(ConfigurationSection permissions, ConfigurationSection difrences){
        if (difrences == null){
            works = false;
            return;
        }
        if (permissions == null){
            works = false;
            return;
        }
        for (String key : permissions.getKeys(false)){
            permissionsRank.put(key.toLowerCase(), permissions.getInt(key, 1));
        }
        for (String key : difrences.getKeys(false)){
            int a = 0;
            try {
                 a = Integer.parseInt(key);
            } catch (NumberFormatException e){
                continue;
            }
            int Item = difrences.getInt(key + ".item", Integer.MIN_VALUE);
            double money = difrences.getDouble(key + ".money", Double.MIN_VALUE);
            if (Item != Integer.MIN_VALUE){
                itemAmountDifrence.put(a, Item);
            }
            if (money != Double.MIN_VALUE){
                moneyDifrence.put(a, money);
            }
        }
    }
    
    public void loadSessionsRewards(ConfigurationSection section){
        if (section == null)
            return;
        for (String key : section.getKeys(false)){
            int a = 0;
            try {
                a = Integer.parseInt(key);
            } catch (NumberFormatException ignored){
                continue;
            }
            int item = section.getInt(key + ".item", Integer.MIN_VALUE);
            double money = section.getDouble(key + ".money", Double.MIN_VALUE);
            List<String> commands = section.getStringList(key + ".commends");
            if (commands != null){
                sessionCommands.put(a, commands);
            }
            if (item != Integer.MIN_VALUE){
                sessionItems.put(a, item);
            }
            if (money != Double.MIN_VALUE){
                sessionMoney.put(a, money);
            }
        }
    }
    
    public String getMaxPermission(Player p, String... groups){
        String perm = "default";
        int max;
        if (permissionsRank.containsKey(perm)){
            max = permissionsRank.get(perm);
        } else {
            max = 0;
        }
        if (p != null){
            if (p.hasPermission("essentials.socialspy")){
                if (permissionsRank.containsKey("svip")){
                    return "svip";
                }
            }
        }
        for (String s : groups){
            s = s.toLowerCase();
            if (permissionsRank.containsKey(s)){
                if (permissionsRank.get(s) > max){
                    max = permissionsRank.get(s);
                    perm = s;
                }
            }
        }
        return perm;
    }
    
}