package pl.minecraft4ever.nagrody;

import java.util.Map;

import com.google.common.collect.Maps;

public class ngrManager {
    
    private Map<SuperSecretClass, Nagroda> lvlngr = Maps.newHashMap();
    private Map<SuperSecretClass, Nagroda> specialngr = Maps.newHashMap();
    
    public void addReward(String lvls ,Nagroda ngr, boolean special){
        int max,min;
        if (special){
            try {
                max = Integer.parseInt(lvls.split("-")[0]);
                min = -2;
            } catch (Exception ignored){
                return;
            }
        } else {
            try {
                min = Integer.parseInt(lvls.split("-")[0]);
                max = Integer.parseInt(lvls.split("-")[1]);
            } catch (Exception ignored){
                return;
            }
        }
        if (special){
            SuperSecretClass clas = new SuperSecretClass(ngr.getPermission().toLowerCase(), max, -2);
            if (specialngr.containsKey(clas)){
                if (specialngr.get(clas).equals(ngr)){
                    return;
                }
                addReward((max + 1) + "", ngr, special);
            } else{
                specialngr.put(clas, ngr);
            }
        } else {
            SuperSecretClass clas = new SuperSecretClass(ngr.getPermission().toLowerCase(), max, min);
            if (lvlngr.containsKey(clas)){
                if (lvlngr.get(clas).equals(ngr))
                    return;
                addReward(min + "-" +(max + 1), ngr, special);
            } else
                lvlngr.put(clas, ngr);
        }
    }
    
    public Nagroda getReward(SuperSecretClass how, int lvl){
        SuperSecretClass two = new SuperSecretClass(how.getPermission(), lvl, -2);
        if (specialngr.containsKey(two)){
            return specialngr.get(two);
        }
        if (lvlngr.containsKey(how)){
            return lvlngr.get(how);
        }
        return null;
    }
    
    /*public void addNgr(SuperSecretClass how, Player player, int lvl, String killed){
        if (lvlngr.containsKey(how)){
            Nagroda n = lvlngr.get(how);
            if (killed != null){
                if (n.hasItems()){
                    try {
                        player.getInventory().addItem(n.getItems());
                        String[] messages = new String[n.getItems().length];
                        for (int i = 0; i < n.getItems().length; i++){
                            ItemStack it = n.getItems()[i];
                            messages[i] = LangConfiguration.getItemNgr().replaceAll("\\{LVL\\}", lvl+"")
                                    .replaceAll("%ilosc%", it.getAmount()+"")
                                    .replaceAll("%item%", it.getType().toString().toLowerCase())
                                    .replaceAll("\\{GRACZ\\}", killed);
                        }
                        player.sendMessage(messages);
                    } catch (Exception ignored){
                        player.sendMessage(LangConfiguration.getNoSlotInInventory().replaceAll("%player%", player.getName()));
                    }
                }
                if (n.hasItem()){
                    try {
                        player.getInventory().addItem(n.getItem());
                        player.sendMessage(LangConfiguration.getItemNgr().replaceAll("\\{LVL\\}", lvl+"")
                                .replaceAll("%ilosc%", n.getItem().getAmount()+"")
                                .replaceAll("%item%", n.getItem().getType().toString().toLowerCase())
                                .replaceAll("\\{GRACZ\\}", killed));
                    } catch (Exception ignored){
                        player.sendMessage(LangConfiguration.getNoSlotInInventory().replaceAll("%player%", player.getName()));
                    }
                }
                if (n.hasPrice()){
                    UltraPvp.getEconomy().depositPlayer(player.getName(), n.getMoney());
                    player.sendMessage(LangConfiguration.getMoneyNgr().replaceAll("\\{LVL\\}", lvl+"")
                            .replaceAll("%kasa%", n.getMoney()+"").replaceAll("\\{GRACZ\\}", killed));
                }
                if (n.hasCommands()){
                    for (String command : n.getCommands()){
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replaceAll("\\{GRACZ\\}", player.getName())
                                .replaceAll("\\{LVL\\}", lvl + "").replaceAll("%kasa%", n.getMoney()+""));
                    }
                }
            } else {
                if (n.hasItems()){
                    try {
                        player.getInventory().addItem(n.getItems());
                    } catch (Exception ignored){
                        player.sendMessage(LangConfiguration.getNoSlotInInventory().replaceAll("%player%", player.getName()));
                    }
                }
                if (n.hasItem()){
                    try {
                        player.getInventory().addItem(n.getItem());
                    } catch (Exception ignored){
                        player.sendMessage(LangConfiguration.getNoSlotInInventory().replaceAll("%player%", player.getName()));
                    }
                }
                if (n.hasPrice()){
                    UltraPvp.getEconomy().depositPlayer(player.getName(), n.getMoney());
                }
                if (n.hasCommands()){
                    for (String command : n.getCommands()){
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replaceAll("\\{GRACZ\\}", player.getName())
                                .replaceAll("\\{LVL\\}", lvl + "").replaceAll("%kasa%", n.getMoney()+""));
                    }
                }
            }
        }
        SuperSecretClass two = new SuperSecretClass(how.getPermission(), lvl, -2);
        if (specialngr.containsKey(two)){
            if (UltraPvp.getInstance().ostatnianagroda.containsKey(player.getName().toLowerCase())){
                if (UltraPvp.getInstance().ostatnianagroda.get(player.getName().toLowerCase()) == lvl){
                    return;
                } else {
                    UltraPvp.getInstance().ostatnianagroda.put(player.getName().toLowerCase(), lvl);
                }
            } else {
                UltraPvp.getInstance().ostatnianagroda.put(player.getName().toLowerCase(), lvl);
            }
            Nagroda n = specialngr.get(two);
            if (n.hasItems()){
                try {
                    player.getInventory().addItem(n.getItems());
                    String[] messages = new String[n.getItems().length];
                    for (int i = 0; i < n.getItems().length; i++){
                        ItemStack it = n.getItems()[i];
                        messages[i] = LangConfiguration.getItemOnceNgr().replaceAll("\\{LVL\\}", lvl+"")
                                .replaceAll("%ilosc%", it.getAmount()+"").replaceAll("%item%", it.getType().toString().toLowerCase());
                    }
                    player.sendMessage(messages);
                } catch (Exception ignored){
                    player.sendMessage(LangConfiguration.getNoSlotInInventory().replaceAll("%player%", player.getName()));
                }
            }
            if (n.hasItem()){
                try {
                    player.getInventory().addItem(n.getItem());
                    player.sendMessage(LangConfiguration.getItemOnceNgr().replaceAll("\\{LVL\\}", lvl+"")
                            .replaceAll("%ilosc%", n.getItem().getAmount()+"")
                            .replaceAll("%item%", n.getItem().getType().toString().toLowerCase()));
                } catch (Exception ignored){
                    player.sendMessage(LangConfiguration.getNoSlotInInventory().replaceAll("%player%", player.getName()));
                }
            }
            if (n.hasPrice()){
                UltraPvp.getEconomy().depositPlayer(player.getName(), n.getMoney());
                player.sendMessage(LangConfiguration.getMoneyOnceNgr().replaceAll("\\{LVL\\}", lvl+"")
                        .replaceAll("%kasa%", n.getMoney()+""));
            }
            if (n.hasCommands()){
                for (String command : n.getCommands()){
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replaceAll("\\{GRACZ\\}", player.getName())
                            .replaceAll("\\{LVL\\}", lvl + "").replaceAll("%kasa%", n.getMoney()+""));
                }
            }
        }
    }*/
    
    /*public void addSpecialNgr(GamePlayer killer, Player player, int lvl, String killed){
        if (killer.getOnceNgr() == null){
            killer.setOnceNgr(getMaxOnceLvlNgr(lvl, killer.getMaxNgr().getPermission()));
            return;
        }
        
    }*/
    
    public SuperSecretClass getMaxLvlNgr(int PlayerLevel, String playerGroup){
        for (SuperSecretClass clas : lvlngr.keySet()){
            if (clas.getPermission().equalsIgnoreCase(playerGroup)){
                if (clas.getMinValue() <= PlayerLevel && clas.getMaxValue() >= PlayerLevel){
                    return clas;
                }
            }
        }
        for (SuperSecretClass clas : lvlngr.keySet()){
            if (clas.getPermission().equalsIgnoreCase("default")){
                if (clas.getMinValue() <= PlayerLevel && clas.getMaxValue() >= PlayerLevel){
                    return clas;
                }
            }
        }
        throw new IllegalStateException("There is no rewards for lvl " + PlayerLevel + " with group " + playerGroup);
    }
    
    /*public SuperSecretClass getMaxOnceLvlNgr(int PlayerLevel, String playerGroup){
        SuperSecretClass min = new SuperSecretClass(playerGroup, PluginConfig.MAX_LVL, -2);
        SuperSecretClass min2 = min;
        for (SuperSecretClass clas : specialngr.keySet()){
            if (clas.getPermission().equalsIgnoreCase(playerGroup)){
                if (clas.getMaxValue() >= PlayerLevel+1 && clas.getMaxValue() < min.getMaxValue()){
                    min = clas;
                }
            }
        }
        for (SuperSecretClass clas : lvlngr.keySet()){
            if (clas.getPermission().equalsIgnoreCase("default")){
                if (clas.getMaxValue() >= PlayerLevel+1 && clas.getMaxValue() < min.getMaxValue()){
                    min = clas;
                }
            }
        }
        if (!min.equals(min2)){
            return min;
        } else
            return new SuperSecretClass(playerGroup, PluginConfig.MAX_LVL, -2);
    }*/
    
    public static class SuperSecretClass {
        
        private int maxlvl, minlvl;
        private String permission;
        
        public SuperSecretClass(String perm, int maxvalue, int minvalue){
            maxlvl = maxvalue;
            minlvl = minvalue;
            permission = perm;
        }
        
        public String getPermission(){
            return permission;
        }
        
        public int getMinValue(){
            return minlvl;
        }
        
        public int getMaxValue(){
            return maxlvl;
        }
        
        @Override
        public boolean equals(Object obj){
            if (obj == null)
                return false;
            if (!(obj instanceof SuperSecretClass))
                return false;
            final SuperSecretClass f  =(SuperSecretClass) obj;
            if (f.maxlvl != this.maxlvl)
                return false;
            if (f.minlvl != this.minlvl)
                return false;
            if (!f.permission.equalsIgnoreCase(this.permission))
                return false;
            return true;
        }
        
        @Override
        public String toString(){
            return "SuperSecretClass[group: " + permission + ", minlvl: " + 
                    minlvl + " , maxlvl: " + maxlvl + "]";
        }
        
        @Override
        public int hashCode(){
            String lvl = new String(maxlvl + "");
            return permission.hashCode() * permission.length() * lvl.length();
        }
        
    }
    
}
