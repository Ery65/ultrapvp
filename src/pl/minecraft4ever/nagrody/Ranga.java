package pl.minecraft4ever.nagrody;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import pl.mc4e.ery65.configuration.PluginConfig;

public class Ranga {
    
    private HashMap<Integer, String> ranga = new HashMap<Integer, String>();
    private HashMap<Integer, Integer> beetwen = new HashMap<Integer, Integer>();
    
    public Ranga(HashMap<String, String> rangi) {
        init(rangi);
    }
    
    private synchronized void init(HashMap<String, String> rangi){
        for (String ranga : rangi.keySet()) {
            String poz = rangi.get(ranga);
            int maxlvl, minlvl;
            String[] dziel = poz.split("-");
            minlvl = Integer.valueOf(dziel[0]);
            maxlvl = minlvl;
            if (dziel.length > 1)
                maxlvl = Integer.valueOf(dziel[1]);
            getRangi(minlvl, maxlvl, ranga);
        }
    }
    
    void getRangi(int minlvl, int maxlvl, String ranga){
        this.ranga.put(maxlvl, ranga);
        beetwen.put(minlvl, maxlvl);
    }
    
    public boolean containsBeetwen(int lvl){
        for (Map.Entry<Integer, Integer> ng : beetwen.entrySet()){
            if (ng.getKey() <= lvl && lvl <= ng.getValue()){
                return true;
            }
        }
        return false;
    }
    
    public String getCorrectRang(int lvl){
        for (Map.Entry<Integer, Integer> rng : beetwen.entrySet()){
           if (rng.getKey() <= lvl && lvl <= rng.getValue()){
               return ranga.get(rng.getValue());
           }
        }
        return null;
    }
    
    public int getMaxValue(int lvl){
        for (Map.Entry<Integer, Integer> rng : beetwen.entrySet()){
            if (rng.getKey() <= lvl && lvl <= rng.getValue()){
                return rng.getValue();
            }
         }
        return lvl;
    }
    
    public String getRang(int lvl){
        return ranga.get(lvl);
    }
    
    public Scoreboard createRangsInScoreboard(Scoreboard board){
        for (Map.Entry<Integer, String> rangs : ranga.entrySet()){
            Team t = board.getTeam(rangs.getValue());
            if (t == null)
                t = board.registerNewTeam(rangs.getValue());
            t.setAllowFriendlyFire(true);
            t.setCanSeeFriendlyInvisibles(false);
            //t.setDisplayName(rangs.getValue());
            t.setPrefix(PluginConfig.RANG_COLOR + rangs.getValue() + " " + PluginConfig.NICK_COLOR);
        }
        return board;
    }

}
