package pl.minecraft4ever.komendy;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pl.mc4e.ery65.configuration.LangConfiguration;
import pl.mc4e.ery65.configuration.PluginConfig;
import pl.minecraft4ever.data.MiniManager;
import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;
import pl.minecraft4ever.nagrody.RangsManager;

public class Ustawianiepoziomu implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (label.equalsIgnoreCase("poziom") || label.equalsIgnoreCase("lvl")) {
            if (args.length == 0) {
                if (cs instanceof Player) {
                    GamePlayer d;
                    if (UltraPvp.getInstance().players.containsKey(cs.getName().toLowerCase())){
                        d = UltraPvp.getInstance().players.get(cs.getName().toLowerCase());
                    } else {
                        d = MiniManager.getCorrectPlayer(new GamePlayer(cs.getName()), (Player) cs);
                    }
                    cs.sendMessage(LangConfiguration.getPlayerLvL().replaceAll("%poziom%", d.getLvL()+""));
                    return true;
                } else {
                    cs.sendMessage(LangConfiguration.getWrongPlayer());
                    return true;
                }
            }
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("reset")) {
                    if (cs instanceof Player) {
                        if (cs.hasPermission("ultrapvp.poziomy.reset")) {
                            if (UltraPvp.getInstance().players.containsKey(cs.getName().toLowerCase())){
                                UltraPvp.getInstance().players.get(cs.getName().toLowerCase()).reset();;
                            } else {
                                MiniManager.getCorrectPlayer(new GamePlayer(cs.getName()), (Player) cs).reset();;
                            }
                            cs.sendMessage(LangConfiguration.getReset());
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getNoPerm());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                } else {
                    if (cs.hasPermission("ultrapvp.poziom.inny")){
                        Player g = Bukkit.getPlayer(args[0]);
                        if (g != null) {
                            GamePlayer d;
                            if (UltraPvp.getInstance().players.containsKey(g.getName().toLowerCase())){
                                d = UltraPvp.getInstance().players.get(g.getName().toLowerCase());
                            } else {
                                d = MiniManager.getCorrectPlayer(new GamePlayer(g.getName()), g);
                            }
                            if (cs == g) {
                                cs.sendMessage(LangConfiguration.getPlayerLvL().replaceAll("%poziom%", d.getLvL()+""));
                                return true;
                            }
                            cs.sendMessage(LangConfiguration.getOtherPlayerLvL()
                                    .replaceAll("\\{GRACZ\\}", d.getName()).replaceAll("%poziom%", d.getLvL()+""));
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
            }
            if (args.length == 2) {
                if (args[0].equalsIgnoreCase("reset")) {
                    Player g = Bukkit.getPlayer(args[1]);
                    if (g != null) {
                        if (!(cs.hasPermission("ultrapvp.poziomy.reset.inny"))) {
                            cs.sendMessage(LangConfiguration.getNoPerm());
                            return true;
                        }
                        GamePlayer d;
                        if (UltraPvp.getInstance().players.containsKey(g.getName().toLowerCase())){
                            d = UltraPvp.getInstance().players.get(g.getName().toLowerCase());
                        } else {
                            d = MiniManager.getCorrectPlayer(new GamePlayer(g.getName()), g);
                        }
                        d.reset();
                        UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                        if (cs == g) {
                            cs.sendMessage(LangConfiguration.getReset());
                            return true;
                        }
                        cs.sendMessage(LangConfiguration.getResetOther().replaceAll("\\{GRACZ\\}", d.getName()));
                        return true;
                    } else {
                        cs.sendMessage(LangConfiguration.getWrongPlayer());
                    }
                }
                if (args[1].equalsIgnoreCase("reset")) {
                    Player g = Bukkit.getPlayer(args[0]);
                    if (g != null) {
                        if (!(cs.hasPermission("ultrapvp.poziomy.reset.inny"))) {
                            cs.sendMessage(LangConfiguration.getNoPerm());
                            return true;
                        }
                        GamePlayer d;
                        if (UltraPvp.getInstance().players.containsKey(g.getName().toLowerCase())){
                            d = UltraPvp.getInstance().players.get(g.getName().toLowerCase());
                        } else {
                            d = MiniManager.getCorrectPlayer(new GamePlayer(g.getName()), g);
                        }
                        d.reset();
                        UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                        if (cs == g) {
                            cs.sendMessage(LangConfiguration.getReset());
                            return true;
                        }
                        cs.sendMessage(LangConfiguration.getResetOther().replaceAll("\\{GRACZ\\}", d.getName()));
                        return true;
                    } else {
                        cs.sendMessage(LangConfiguration.getWrongPlayer());
                    }
                } else {
                    cs.sendMessage(LangConfiguration.getLvLUsage());
                    return true;
                }
            }
            if (args.length == 3) {
                if (args[1].equalsIgnoreCase("ustaw") || args[1].equalsIgnoreCase("set")) {
                    if (cs.hasPermission("ultrapvp.poziomy.ustaw")) {
                        Player g = Bukkit.getPlayer(args[0]);
                        if (g != null) {
                            GamePlayer d;
                            if (UltraPvp.getInstance().players.containsKey(g.getName().toLowerCase())){
                                d = UltraPvp.getInstance().players.get(g.getName().toLowerCase());
                            } else {
                                d = MiniManager.getCorrectPlayer(new GamePlayer(g.getName()), g);
                            }
                            int ust;
                            try {
                                ust = Integer.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            if (ust < 0){
                                cs.sendMessage(LangConfiguration.getWrongLvL().replaceAll("\\{LVL\\}", ust+""));
                                return true;
                            }
                            if (ust > PluginConfig.MAX_LVL){
                                cs.sendMessage(LangConfiguration.getTooLargeLvL());
                                return true;
                            }
                            d.setCorrectLvL(ust);
                            UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                            UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                            RangsManager r = UltraPvp.getRang();
                            if (r.containsBeetwen(d.getLvL())){
                                d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                if (PluginConfig.USE_RANG_BELLOW_NAME){
                                    UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                }
                                d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                            }
                            cs.sendMessage(LangConfiguration.getSucccesSetLvL()
                                    .replaceAll("\\{LVL\\}", ust+"").replaceAll("\\{GRACZ\\}", d.getName()));
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[0].equalsIgnoreCase("ustaw") || args[0].equalsIgnoreCase("set")) {
                    if (cs.hasPermission("ultrapvp.poziomy.ustaw")) {
                        Player g = Bukkit.getPlayer(args[1]);
                        if (g != null) {
                            GamePlayer d;
                            if (UltraPvp.getInstance().players.containsKey(g.getName().toLowerCase())){
                                d = UltraPvp.getInstance().players.get(g.getName().toLowerCase());
                            } else {
                                d = MiniManager.getCorrectPlayer(new GamePlayer(g.getName()), g);
                            }
                            int ust;
                            try {
                                ust = Integer.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            ust = Integer.valueOf(args[2]);
                            if (ust < 0){
                                cs.sendMessage(LangConfiguration.getWrongLvL().replaceAll("\\{LVL\\}", ust+""));
                                return true;
                            }
                            if (ust > PluginConfig.MAX_LVL){
                                cs.sendMessage(LangConfiguration.getTooLargeLvL());
                                return true;
                            }
                            d.setCorrectLvL(ust);
                            UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                            RangsManager r = UltraPvp.getRang();
                            if (r.containsBeetwen(d.getLvL())){
                                d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                if (PluginConfig.USE_RANG_BELLOW_NAME){
                                    UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                }
                                d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                            }
                            cs.sendMessage(LangConfiguration.getSucccesSetLvL()
                                    .replaceAll("\\{LVL\\}", ust+"").replaceAll("\\{GRACZ\\}", d.getName()));
                            UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[0].equalsIgnoreCase("dodaj") || args[0].equalsIgnoreCase("add")){
                    if (cs.hasPermission("ultrapvp.poziomy.dodaj")) {
                        Player g = Bukkit.getPlayer(args[1]);
                        if (g != null) {
                            GamePlayer d;
                            if (UltraPvp.getInstance().players.containsKey(g.getName().toLowerCase())){
                                d = UltraPvp.getInstance().players.get(g.getName().toLowerCase());
                            } else {
                                d = MiniManager.getCorrectPlayer(new GamePlayer(g.getName()), g);
                            }
                            int dodanie;
                            try {
                                dodanie = Integer.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            if (dodanie < 0) {
                                dodanie = dodanie * (-1);
                                int ostpoz = d.getLvL() - dodanie;
                                if (ostpoz < 0) {
                                    cs.sendMessage(LangConfiguration.getWrongLvL().replaceAll("\\{LVL\\}", ostpoz+""));
                                    return true;
                                }
                                d.setCorrectLvL(ostpoz);
                                UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                                RangsManager r = UltraPvp.getRang();
                                if (r.containsBeetwen(d.getLvL())){
                                    d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                    if (PluginConfig.USE_RANG_BELLOW_NAME){
                                        UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                    }
                                    d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                    d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                    d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                                }
                                cs.sendMessage(LangConfiguration.getSuccesRem().replaceAll("\\{LVL\\}", dodanie+"")
                                        .replaceAll("\\{GRACZ\\}", g.getName()));
                                UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                                return true;
                            }
                            int ostpoz = d.getLvL() + dodanie;
                            if (ostpoz > PluginConfig.MAX_LVL) {
                                cs.sendMessage(LangConfiguration.getTooLargeLvL());
                                return true;
                            }
                            for (int i = d.getLvL(); i <= ostpoz;i++){
                                d.addZezw(1);
                                if (UltraPvp.getRewardManager().containsAllow(d.getZezw())){
                                    d.addLvL(1);
                                    if (d.getMaxNgr().getMaxValue() == (d.getLvL())){
                                        d = MiniManager.getCorrectPlayer(d, g);
                                    }
                                    if (d.getMaxCurrentRangLvL() < d.getLvL()){
                                        if (UltraPvp.getRang().containsBeetwen(d.getLvL())){
                                            d.setMaxCurrentLvLRang(d.getLvLNextRang());
                                            d.SetRang(d.getNextRang());
                                            d.setNextRangLvL(UltraPvp.getRang().getMaxValue(d.getLvL()));
                                            d.setNextRang(UltraPvp.getRang().getCorrectRang(d.getLvLNextRang()));
                                        }
                                    }
                                    //TODO
                                    /*UltraPvp.getNgrManager().addNgr(d.getMaxNgr(), g,
                                            d.getLvL(), null);*/
                                }
                            }
                            d.setCorrectLvL(ostpoz);
                            UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                            RangsManager r = UltraPvp.getRang();
                            if (r.containsBeetwen(d.getLvL())){
                                d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                if (PluginConfig.USE_RANG_BELLOW_NAME){
                                    UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                }
                                d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                            }
                            cs.sendMessage(LangConfiguration.getSuccesAdd().replaceAll("\\{LVL\\}", dodanie+"")
                                    .replaceAll("\\{GRACZ\\}", g.getName()));
                            UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[1].equalsIgnoreCase("dodaj") || args[1].equalsIgnoreCase("add")){
                    if (cs.hasPermission("ultrapvp.poziomy.dodaj")) {
                        Player g = Bukkit.getPlayer(args[0]);
                        if (g != null) {
                            GamePlayer d;
                            if (UltraPvp.getInstance().players.containsKey(g.getName().toLowerCase())){
                                d = UltraPvp.getInstance().players.get(g.getName().toLowerCase());
                            } else {
                                d = MiniManager.getCorrectPlayer(new GamePlayer(g.getName()), g);
                            }
                            int dodanie;
                            try {
                                dodanie = Integer.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            dodanie = Integer.valueOf(args[2]);
                            if (dodanie < 0) {
                                dodanie = dodanie * (-1);
                                int ostpoz = d.getLvL() - dodanie;
                                if (ostpoz < 0) {
                                    cs.sendMessage(LangConfiguration.getWrongLvL().replaceAll("\\{LVL\\}", ostpoz+""));
                                    return true;
                                }
                                d.setCorrectLvL(ostpoz);
                                UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                                RangsManager r = UltraPvp.getRang();
                                if (r.containsBeetwen(d.getLvL())){
                                    d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                    if (PluginConfig.USE_RANG_BELLOW_NAME){
                                        UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                    }
                                    d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                    d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                    d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                                }
                                UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                                cs.sendMessage(LangConfiguration.getSuccesRem().replaceAll("\\{LVL\\}", dodanie+"")
                                        .replaceAll("\\{GRACZ\\}", g.getName()));
                                return true;
                            }
                            int ostpoz = d.getLvL() + dodanie;
                            if (ostpoz > PluginConfig.MAX_LVL) {
                                cs.sendMessage(LangConfiguration.getTooLargeLvL());
                                return true;
                            }
                            for (int i = d.getLvL(); i <= ostpoz;i++){
                                d.addZezw(1);
                                if (UltraPvp.getRewardManager().containsAllow(d.getZezw())){
                                    d.addLvL(1);
                                    if (d.getMaxNgr().getMaxValue() == (d.getLvL())){
                                        d = MiniManager.getCorrectPlayer(d, g);
                                    }
                                    if (d.getMaxCurrentRangLvL() < d.getLvL()){
                                        if (UltraPvp.getRang().containsBeetwen(d.getLvL())){
                                            d.setMaxCurrentLvLRang(d.getLvLNextRang());
                                            d.SetRang(d.getNextRang());
                                            d.setNextRangLvL(UltraPvp.getRang().getMaxValue(d.getLvL()));
                                            d.setNextRang(UltraPvp.getRang().getCorrectRang(d.getLvLNextRang()));
                                        }
                                    }
                                    //TODO
                                    /*UltraPvp.getNgrManager().addNgr(d.getMaxNgr(), g,
                                            d.getLvL(), null);*/
                                }
                            }
                            d.setCorrectLvL(ostpoz);
                            UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                            RangsManager r = UltraPvp.getRang();
                            if (r.containsBeetwen(d.getLvL())){
                                d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                if (PluginConfig.USE_RANG_BELLOW_NAME){
                                    UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                }
                                d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                            }
                            cs.sendMessage(LangConfiguration.getSuccesAdd().replaceAll("\\{LVL\\}", dodanie+"")
                                    .replaceAll("\\{GRACZ\\}", g.getName()));
                            UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[1].equalsIgnoreCase("odejmij") || args[1].equalsIgnoreCase("remove")){
                    if (cs.hasPermission("ultrapvp.poziomy.odejmij")){
                        Player g = Bukkit.getPlayer(args[0]);
                        if (g != null) {
                            GamePlayer d;
                            if (UltraPvp.getInstance().players.containsKey(g.getName().toLowerCase())){
                                d = UltraPvp.getInstance().players.get(g.getName().toLowerCase());
                            } else {
                                d = MiniManager.getCorrectPlayer(new GamePlayer(g.getName()), g);
                            }
                            int odejmij;
                            try {
                                odejmij = Integer.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            odejmij = Integer.valueOf(args[2]);
                            int ostpoz;
                            if (odejmij < 0) {
                                odejmij = odejmij * (-1);
                                ostpoz = d.getLvL() + odejmij;
                                if (ostpoz > PluginConfig.MAX_LVL) {
                                    cs.sendMessage(LangConfiguration.getTooLargeLvL());
                                    return true;
                                }
                                d.setCorrectLvL(ostpoz);
                                UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                                RangsManager r = UltraPvp.getRang();
                                if (r.containsBeetwen(d.getLvL())){
                                    d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                    if (PluginConfig.USE_RANG_BELLOW_NAME){
                                        UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                    }
                                    d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                    d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                    d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                                }
                                UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                                cs.sendMessage(LangConfiguration.getSuccesAdd().replaceAll("\\{LVL\\}", odejmij +"")
                                        .replaceAll("\\{GRACZ\\}", g.getName()));
                                return true;
                            }
                            ostpoz = d.getLvL() - odejmij;
                            if (ostpoz < 0) {
                                cs.sendMessage(LangConfiguration.getWrongLvL().replaceAll("\\{LVL\\}", ostpoz+""));
                                return true;
                            }
                            d.setCorrectLvL(ostpoz);
                            UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                            RangsManager r = UltraPvp.getRang();
                            if (r.containsBeetwen(d.getLvL())){
                                d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                if (PluginConfig.USE_RANG_BELLOW_NAME){
                                    UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                }
                                d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                            }
                            cs.sendMessage(LangConfiguration.getSuccesRem().replaceAll("\\{LVL\\}", odejmij +"")
                                    .replaceAll("\\{GRACZ\\}", g.getName()));
                            UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[0].equalsIgnoreCase("odejmij") || args[0].equalsIgnoreCase("remove")){
                    if (cs.hasPermission("ultrapvp.poziomy.odejmij")){
                        Player g = Bukkit.getPlayer(args[1]);
                        if (g != null) {
                            GamePlayer d;
                            if (UltraPvp.getInstance().players.containsKey(g.getName().toLowerCase())){
                                d = UltraPvp.getInstance().players.get(g.getName().toLowerCase());
                            } else {
                                d = MiniManager.getCorrectPlayer(new GamePlayer(g.getName()), g);
                            }
                            int odejmij;
                            try {
                                odejmij = Integer.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            odejmij = Integer.valueOf(args[2]);
                            int ostpoz;
                            if (odejmij < 0) {
                                odejmij = odejmij * (-1);
                                ostpoz = d.getLvL() + odejmij;
                                if (ostpoz > PluginConfig.MAX_LVL) {
                                    cs.sendMessage(LangConfiguration.getTooLargeLvL());
                                    return true;
                                }
                                d.setCorrectLvL(ostpoz);
                                UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                                RangsManager r = UltraPvp.getRang();
                                if (r.containsBeetwen(d.getLvL())){
                                    d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                    if (PluginConfig.USE_RANG_BELLOW_NAME){
                                        UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                    }
                                    d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                    d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                    d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                                }
                                UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                                cs.sendMessage(LangConfiguration.getSuccesAdd().replaceAll("\\{LVL\\}", odejmij+"")
                                        .replaceAll("\\{GRACZ\\}", g.getName()));
                                return true;
                            }
                            ostpoz = d.getLvL() - odejmij;
                            if (ostpoz < 0) {
                                cs.sendMessage(LangConfiguration.getWrongLvL().replaceAll("\\{LVL\\}", ostpoz+""));
                                return true;
                            }
                            d.setCorrectLvL(ostpoz);
                            UltraPvp.getSimpleScoreboardManager().updateLvlObjective(g, d.getLvL());
                            RangsManager r = UltraPvp.getRang();
                            if (r.containsBeetwen(d.getLvL())){
                                d.setMaxCurrentLvLRang(r.getMaxValue(d.getLvL()));
                                if (PluginConfig.USE_RANG_BELLOW_NAME){
                                    UltraPvp.getSimpleScoreboardManager().updateRangObjective(g, r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                }
                                d.SetRang(r.getCorrectRang(d.getMaxCurrentRangLvL()));
                                d.setNextRangLvL(r.getMaxValue(d.getMaxCurrentRangLvL()+1));
                                d.setNextRang(r.getCorrectRang(d.getLvLNextRang()));
                            }
                            UltraPvp.getInstance().players.put(d.getName().toLowerCase(), d);
                            cs.sendMessage(LangConfiguration.getSuccesRem().replaceAll("\\{LVL\\}", odejmij+"")
                                    .replaceAll("\\{GRACZ\\}", g.getName()));
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                } else {
                    cs.sendMessage(LangConfiguration.getLvLUsage());
                }
                
            } else {
                cs.sendMessage(LangConfiguration.getLvLUsage());
            }
        }
        return true;
    }

}
