package pl.minecraft4ever.ekonomia;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pl.mc4e.ery65.configuration.LangConfiguration;
import pl.mc4e.ery65.configuration.PluginConfig;
import pl.minecraft4ever.glowna.UltraPvp;

public class Kasa implements CommandExecutor {

    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (label.equalsIgnoreCase("kasa") || label.equalsIgnoreCase("money")) {
            if (args.length == 0) {
                if (cs instanceof Player) {
                    double wartosc = UltraPvp.getEconomy().getBalance((Player) cs);
                    cs.sendMessage(LangConfiguration.getYourBalance().replaceAll("%kasa%", wartosc+""));
                    return true;
                } else {
                    cs.sendMessage(LangConfiguration.getCompWarnEconomy());
                    return true;
                }
            }
            
            if (args.length == 1) {
                Player g = Bukkit.getPlayer(args[0]);
                if (g != null) {
                    double stan = UltraPvp.getEconomy().getBalance(g);
                    if (g == cs){
                        cs.sendMessage(LangConfiguration.getYourBalance().replaceAll("%kasa%", stan+""));
                        return true;
                    }
                    cs.sendMessage(LangConfiguration.getOtherBalance().replaceAll("\\{GRACZ\\}", g.getName())
                            .replaceAll("%kasa%", stan+""));
                    return true;
                } else {
                    cs.sendMessage(LangConfiguration.getCompWarnEconomy());
                    return true;
                }
            }
            
            if (args.length == 2) {
                if (args[1].equalsIgnoreCase("reset")){
                    if (cs.hasPermission("ultrapvp.poziom.reset")){
                        Player g = Bukkit.getPlayer(args[0]);
                        if (g != null) {
                            double stan = UltraPvp.getEconomy().getBalance(g);
                            UltraPvp.getEconomy().withdrawPlayer(g, stan);
                            cs.sendMessage(LangConfiguration.getResetBalance().replaceAll("\\{GRACZ\\}", g.getName()));
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[0].equalsIgnoreCase("reset")){
                    if (cs.hasPermission("ultrapvp.poziom.reset")){
                        Player g = Bukkit.getPlayer(args[1]);
                        if (g != null) {
                            double stan = UltraPvp.getEconomy().getBalance(g);
                            UltraPvp.getEconomy().withdrawPlayer(g, stan);
                            cs.sendMessage(LangConfiguration.getResetBalance().replaceAll("\\{GRACZ\\}", g.getName()));
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                } else {
                    cs.sendMessage(LangConfiguration.getEconUsage());
                    return true;
                }
            }
            
            if (args.length == 3) {
                if (args[1].equalsIgnoreCase("dodaj") || args[1].equalsIgnoreCase("add")){
                    if (cs.hasPermission("ultrapvp.ekon.dodaj")){
                        Player g = Bukkit.getPlayer(args[0]);
                        if (g != null){
                            double dodanie;
                            try{
                                dodanie = Double.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            dodanie = Double.valueOf(args[2]);
                            if (dodanie < 0) {
                                double dod1 = dodanie*(-1);
                                double b = UltraPvp.getEconomy().getBalance(g);
                                double ostb = b-dod1;
                                if (ostb < 0) {
                                    if (PluginConfig.USE_NEGATIVE_BALANCE) {
                                        UltraPvp.getEconomy().withdrawPlayer(g, dod1);
                                        cs.sendMessage(LangConfiguration.getSuccesRemEcon()
                                                .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", dod1+""));
                                        return true;
                                    }
                                    cs.sendMessage(LangConfiguration.getWrongBalance().replaceAll("%kasa%", ostb + ""));
                                    return true;
                                }
                                cs.sendMessage(LangConfiguration.getSuccesRemEcon()
                                        .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", dod1+""));
                                return true;
                            }
                            UltraPvp.getEconomy().depositPlayer(g, dodanie);
                            cs.sendMessage(LangConfiguration.getSuccesAddEcon()
                                    .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", dodanie+""));
                            return true;
                        } else{
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[0].equalsIgnoreCase("dodaj") || args[0].equalsIgnoreCase("add")){
                    if (cs.hasPermission("ultrapvp.ekon.dodaj")){
                        Player g = Bukkit.getPlayer(args[1]);
                        if (g != null){
                            double dodanie;
                            try{
                                dodanie = Double.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            dodanie = Double.valueOf(args[2]);
                            if (dodanie < 0) {
                                double dod1 = dodanie*(-1);
                                double b = UltraPvp.getEconomy().getBalance(g);
                                double ostb = b-dod1;
                                if (ostb < 0) {
                                    if (PluginConfig.USE_NEGATIVE_BALANCE) {
                                        UltraPvp.getEconomy().withdrawPlayer(g, dod1);
                                        cs.sendMessage(LangConfiguration.getSuccesRemEcon()
                                                .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", dod1+""));
                                        return true;
                                    }
                                    cs.sendMessage(LangConfiguration.getWrongBalance().replaceAll("%kasa%", ostb + ""));
                                    return true;
                                }
                                cs.sendMessage(LangConfiguration.getSuccesRemEcon()
                                        .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", dod1+""));
                                return true;
                            }
                            UltraPvp.getEconomy().depositPlayer(g, dodanie);
                            cs.sendMessage(LangConfiguration.getSuccesAddEcon()
                                    .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", dodanie+""));
                            return true;
                        } else{
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[1].equalsIgnoreCase("odejmij") || args[1].equalsIgnoreCase("remove")){
                    if (cs.hasPermission("ultrapvp.ekon.odejmij")) {
                        Player g = Bukkit.getPlayer(args[0]);
                        if (g != null) {
                            double odejmij;
                            try {
                                odejmij = Double.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            odejmij = Double.valueOf(args[2]);
                            if (odejmij < 0) {
                                double odj1 = odejmij*(-1);
                                UltraPvp.getEconomy().depositPlayer(g, odj1);
                                cs.sendMessage(LangConfiguration.getSuccesAddEcon()
                                        .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", odj1+""));
                                return true;
                            }
                            double balans = UltraPvp.getEconomy().getBalance(g);
                            double ostb = balans-odejmij;
                            if (ostb < 0){
                                if (PluginConfig.USE_NEGATIVE_BALANCE) {
                                    UltraPvp.getEconomy().withdrawPlayer(g, odejmij);
                                    cs.sendMessage(LangConfiguration.getSuccesRemEcon()
                                            .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", odejmij+""));
                                    return true;
                                }
                                cs.sendMessage(LangConfiguration.getWrongBalance().replaceAll("%kasa%", ostb + ""));
                                return true;
                            }
                            UltraPvp.getEconomy().withdrawPlayer(g, odejmij);
                            cs.sendMessage(LangConfiguration.getSuccesRemEcon()
                                    .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", odejmij+""));
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[0].equalsIgnoreCase("odejmij") || args[0].equalsIgnoreCase("remove")){
                    if (cs.hasPermission("ultrapvp.ekon.odejmij")) {
                        Player g = Bukkit.getPlayer(args[1]);
                        if (g != null) {
                            double odejmij;
                            try {
                                odejmij = Double.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            odejmij = Double.valueOf(args[2]);
                            if (odejmij < 0) {
                                double odj1 = odejmij*(-1);
                                UltraPvp.getEconomy().depositPlayer(g, odj1);
                                cs.sendMessage(LangConfiguration.getSuccesAddEcon()
                                        .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", odj1+""));
                                return true;
                            }
                            double balans = UltraPvp.getEconomy().getBalance(g);
                            double ostb = balans-odejmij;
                            if (ostb < 0){
                                if (PluginConfig.USE_NEGATIVE_BALANCE) {
                                    UltraPvp.getEconomy().withdrawPlayer(g, odejmij);
                                    cs.sendMessage(LangConfiguration.getSuccesRemEcon()
                                            .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", odejmij+""));
                                    return true;
                                }
                                cs.sendMessage(LangConfiguration.getWrongBalance().replaceAll("%kasa%", ostb + ""));
                                return true;
                            }
                            UltraPvp.getEconomy().withdrawPlayer(g, odejmij);
                            cs.sendMessage(LangConfiguration.getSuccesRemEcon()
                                    .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", odejmij+""));
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[1].equalsIgnoreCase("przelej") || args[1].equalsIgnoreCase("pay") || args[1].equalsIgnoreCase("zaplac")){
                    if (cs instanceof Player){
                        if (cs.hasPermission("ultrapvp.ekon.przelej")){
                            Player g = Bukkit.getPlayer(args[0]);
                            if (g != null) {
                                double przel;
                                try {
                                    przel = Double.valueOf(args[2]);
                                } catch (NumberFormatException e) {
                                    cs.sendMessage(LangConfiguration.getWrongValue());
                                    return true;
                                }
                                przel = Double.valueOf(args[2]);
                                if (cs == g){
                                    cs.sendMessage(LangConfiguration.getYourselfPayment());
                                    return true;
                                }
                                if (przel <= 0) {
                                    cs.sendMessage(LangConfiguration.getWrongPayment());
                                    return true;
                                }
                                if (UltraPvp.getEconomy().getBalance((Player) cs) < przel){
                                    cs.sendMessage(LangConfiguration.getWrongBalance().replaceAll(
                                            "%kasa%", UltraPvp.getEconomy().getBalance((Player) cs) - przel + ""));
                                    return true;
                                }
                                UltraPvp.getEconomy().withdrawPlayer((Player) cs, przel);
                                UltraPvp.getEconomy().depositPlayer(g, przel);
                                cs.sendMessage(LangConfiguration.getPaymentToPlayer()
                                        .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", args[2]));
                                g.sendMessage(LangConfiguration.getPaymentFromPlayer()
                                        .replaceAll("\\{GRACZ\\}", cs.getName()).replaceAll("%kasa%", args[2]));
                                return true;
                            } else {
                                cs.sendMessage(LangConfiguration.getWrongPlayer());
                                return true;
                            }
                        } else {
                            cs.sendMessage(LangConfiguration.getNoPerm());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getCompPayment());
                        return true;
                    }
                }
                
                if (args[0].equalsIgnoreCase("przelej") || args[0].equalsIgnoreCase("pay") || args[0].equalsIgnoreCase("zaplac")){
                    if (cs instanceof Player){
                        if (cs.hasPermission("ultrapvp.ekon.przelej")){
                            Player g = Bukkit.getPlayer(args[1]);
                            if (g != null) {
                                double przel;
                                try {
                                    przel = Double.valueOf(args[2]);
                                } catch (NumberFormatException e) {
                                    cs.sendMessage(LangConfiguration.getWrongValue());
                                    return true;
                                }
                                przel = Double.valueOf(args[2]);
                                if (cs == g){
                                    cs.sendMessage(LangConfiguration.getYourselfPayment());
                                    return true;
                                }
                                if (przel <= 0) {
                                    cs.sendMessage(LangConfiguration.getWrongPayment());
                                    return true;
                                }
                                if (UltraPvp.getEconomy().getBalance((Player) cs) < przel){
                                    cs.sendMessage(LangConfiguration.getWrongBalance().replaceAll(
                                            "%kasa%", UltraPvp.getEconomy().getBalance((Player) cs) - przel + ""));
                                    return true;
                                }
                                UltraPvp.getEconomy().withdrawPlayer((Player) cs, przel);
                                UltraPvp.getEconomy().depositPlayer(g, przel);
                                cs.sendMessage(LangConfiguration.getPaymentToPlayer()
                                        .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", args[2]));
                                g.sendMessage(LangConfiguration.getPaymentFromPlayer()
                                        .replaceAll("\\{GRACZ\\}", cs.getName()).replaceAll("%kasa%", args[2]));
                                return true;
                            } else {
                                cs.sendMessage(LangConfiguration.getWrongPlayer());
                                return true;
                            }
                        } else {
                            cs.sendMessage(LangConfiguration.getNoPerm());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getCompPayment());
                        return true;
                    }
                }
                
                if (args[1].equalsIgnoreCase("ustaw") || args[1].equalsIgnoreCase("set")){
                    if (cs.hasPermission("ultrapvp.ekon.ustaw")){
                        Player g = Bukkit.getPlayer(args[0]);
                        if (g != null) {
                            double ust;
                            try {
                                ust = Double.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            ust = Double.valueOf(args[2]);
                            double b = UltraPvp.getEconomy().getBalance(g);
                            if (ust < 0) {
                                if (PluginConfig.USE_NEGATIVE_BALANCE) {
                                    double ust1 = ust*(-1);
                                    double b2 = b+ust1;
                                    UltraPvp.getEconomy().withdrawPlayer(g, b2);
                                    cs.sendMessage(LangConfiguration.getSuccesSetEcon()
                                            .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", args[2]));
                                    return true;
                                }
                                cs.sendMessage(LangConfiguration.getWrongBalance().replaceAll("%kasa%", args[2] + ""));
                                return true;
                            }
                            UltraPvp.getEconomy().withdrawPlayer(g, b);
                            UltraPvp.getEconomy().depositPlayer(g, ust);
                            cs.sendMessage(LangConfiguration.getSuccesSetEcon().replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", args[2]));
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                }
                
                if (args[0].equalsIgnoreCase("ustaw") || args[0].equalsIgnoreCase("set")){
                    if (cs.hasPermission("ultrapvp.ekon.ustaw")){
                        Player g = Bukkit.getPlayer(args[1]);
                        if (g != null) {
                            double ust;
                            try {
                                ust = Double.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                cs.sendMessage(LangConfiguration.getWrongValue());
                                return true;
                            }
                            ust = Double.valueOf(args[2]);
                            double b = UltraPvp.getEconomy().getBalance(g);
                            if (ust < 0) {
                                if (PluginConfig.USE_NEGATIVE_BALANCE) {
                                    double ust1 = ust*(-1);
                                    double b2 = b+ust1;
                                    UltraPvp.getEconomy().withdrawPlayer(g, b2);
                                    cs.sendMessage(LangConfiguration.getSuccesSetEcon().replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", args[2]));
                                    return true;
                                }
                                cs.sendMessage(LangConfiguration.getWrongBalance().replaceAll("%kasa%", args[2] + ""));
                                return true;
                            }
                            UltraPvp.getEconomy().withdrawPlayer(g, b);
                            UltraPvp.getEconomy().depositPlayer(g, ust);
                            cs.sendMessage(LangConfiguration.getSuccesSetEcon()
                                    .replaceAll("\\{GRACZ\\}", g.getName()).replaceAll("%kasa%", args[2]));
                            return true;
                        } else {
                            cs.sendMessage(LangConfiguration.getWrongPlayer());
                            return true;
                        }
                    } else {
                        cs.sendMessage(LangConfiguration.getNoPerm());
                        return true;
                    }
                } else {
                    cs.sendMessage(LangConfiguration.getEconUsage());
                    return true;
                }
                
                
            } else {
                cs.sendMessage(LangConfiguration.getEconUsage());
            }
            
        }
        return true;
    }
}
