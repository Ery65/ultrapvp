package pl.mc4e.ery65.ultrapvp.utils;

import java.util.Calendar;
import java.util.Date;

public class DateHelper {
    
    public static Date removeDays(int days){
        Calendar cal = Calendar.getInstance();
        if (days < 365){
            if (cal.get(Calendar.DAY_OF_YEAR) < days){
                int yeahr = cal.get(Calendar.YEAR);
                int yeahr_ = yeahr -1;
                if (yeahr_ % 4 == 0){
                    cal.set(Calendar.YEAR, yeahr_);
                    cal.set(Calendar.DAY_OF_YEAR, 366 - ( -(cal.get(Calendar.DAY_OF_YEAR) - days)));
                } else {
                    cal.set(Calendar.YEAR, yeahr_);
                    cal.set(Calendar.DAY_OF_YEAR, 365 - ( -(cal.get(Calendar.DAY_OF_YEAR) - days)));
                }
            } else {
                cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - days);
            }
        } else {
            int rest = days % 365;
            int howmany = (days - rest) / 365;
            if (howmany == 0){
                if (((cal.get(Calendar.YEAR) -1) % 4) == 0) {
                    cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) -1);
                    cal.set(Calendar.DAY_OF_YEAR, 366 - (-(cal.get(Calendar.DAY_OF_YEAR) - days)));
                } else {
                    cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) -1);
                    cal.set(Calendar.DAY_OF_YEAR, 366 - (-(cal.get(Calendar.DAY_OF_YEAR) - days)));
                }
            } else {
                System.out.println(1);
                int yeahr = cal.get(Calendar.YEAR);
                int how = getLenintYeahr(yeahr);
                /*if (how < howmany){
                    int many = (howmany - how) / 4;
                    cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                    cal.set(Calendar.DAY_OF_YEAR, (cal.get(Calendar.DAY_OF_YEAR) ));
                } else if (how == howmany){
                    
                } else {
                    
                }*/
                int day = cal.get(Calendar.DAY_OF_YEAR);
                if (day - rest < 0){
                    if (how < howmany){
                        int many = (howmany - how) / 4;
                        days += many;
                        if (days > 365) {
                            days = 365 - days;
                            howmany++;
                            if (getLenintYeahr(howmany) == 0){
                                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                                cal.set(Calendar.DAY_OF_YEAR, 366 -(-(cal.get(Calendar.DAY_OF_YEAR) - rest)));
                            } else {
                                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                                cal.set(Calendar.DAY_OF_YEAR, 365 -(-(cal.get(Calendar.DAY_OF_YEAR) - rest)));
                            }
                        } else {
                            if (getLenintYeahr(howmany) == 0){
                                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                                cal.set(Calendar.DAY_OF_YEAR, 366 -(-(cal.get(Calendar.DAY_OF_YEAR) - rest)));
                            } else {
                                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                                cal.set(Calendar.DAY_OF_YEAR, 365 -(-(cal.get(Calendar.DAY_OF_YEAR) - rest)));
                            }
                        }
                    } else if (how == howmany){
                        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                        cal.set(Calendar.DAY_OF_YEAR, 366 -(-(cal.get(Calendar.DAY_OF_YEAR) - rest)));
                    } else {
                        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                        cal.set(Calendar.DAY_OF_YEAR, 365 -(-(cal.get(Calendar.DAY_OF_YEAR) - rest)));
                    }  
                } else {
                    if (how < howmany){
                        int many = (howmany - how) / 4;
                        days += many;
                        if (days > 365) {
                            days = 365 - days;
                            howmany++;
                            if (getLenintYeahr(howmany) == 0){
                                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                                cal.set(Calendar.DAY_OF_YEAR, (cal.get(Calendar.DAY_OF_YEAR) - rest));
                            } else {
                                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                                cal.set(Calendar.DAY_OF_YEAR, (cal.get(Calendar.DAY_OF_YEAR) - rest));
                            }
                        } else {
                            if (getLenintYeahr(howmany) == 0){
                                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                                cal.set(Calendar.DAY_OF_YEAR, (cal.get(Calendar.DAY_OF_YEAR) - rest));
                            } else {
                                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                                cal.set(Calendar.DAY_OF_YEAR, (cal.get(Calendar.DAY_OF_YEAR) - rest));
                            }
                        }
                    } else if (how == howmany){
                        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                        cal.set(Calendar.DAY_OF_YEAR, (cal.get(Calendar.DAY_OF_YEAR) - rest));
                    } else {
                        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - howmany);
                        cal.set(Calendar.DAY_OF_YEAR, (cal.get(Calendar.DAY_OF_YEAR) - rest));
                    }  
                }
            }
        }
        return cal.getTime();
    }
    
    private static int getLenintYeahr(int yeahr){
        if (yeahr % 4 == 0){
            return 0;
        } else if ((yeahr + 1) % 4 == 0){
            return 1;
        } else if ((yeahr + 2) % 4 == 0){
            return 2;
        } else {
            return 3;
        }
    }
    
}
