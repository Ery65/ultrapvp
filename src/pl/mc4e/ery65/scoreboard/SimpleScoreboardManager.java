package pl.mc4e.ery65.scoreboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import pl.mc4e.ery65.configuration.PluginConfig;
import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;

import com.google.common.collect.Maps;

public class SimpleScoreboardManager {
    
    private Map<String, String> translations = Maps.newHashMap();
    private Map<Player, SimpleCraftScoreboard> playerScoreboard = new HashMap<Player, SimpleCraftScoreboard>();
    
    private Scoreboard blankScoreboard;
    
    private SimpleCraftScoreboard mainSb;
    
    private Timer tim;
    
    private List<HashMap<String, String>> objectives = new ArrayList<HashMap<String, String>>();
    
    private int currentObjective = 0;
    
    private long currentObjTime = -1;
    private long elapsedTime = 0;
    private long refreshTime = -1;
    
    public SimpleScoreboardManager(){
        tim = new Timer();
        blankScoreboard = UltraPvp.getInstance().getServer().getScoreboardManager().getNewScoreboard();
        if (PluginConfig.ENABLE_SCOREBOARDS){
            encodeSidebar(PluginConfig.getConfigurationSection("Scoreboard.sidebar.scores"));
            loadScoreboards();
        } else {
            dispose();
        }
    }
    
    /*    private synchronized SimpleCraftObjective encodeNameAndTimes(SimpleCraftObjective obj){
        HashMap<String, String> current = objectives.get(currentObjective);
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName(current.get("ScoreboardObjectiveName"));
        currentObjTime = parseLong(current.get("ScoreboardObjectiveTime"));
        refreshTime = parseLong(current.get("ScoreboardObjectiveRefreshTime"));
        return obj;
    }*/
    
    private synchronized SimpleCraftObjective encodeNameAndTimes(SimpleCraftObjective obj, boolean toggle){
        if (toggle){
            HashMap<String, String> current = objectives.get(currentObjective);
            obj.setDisplaySlot(DisplaySlot.SIDEBAR);
            obj.setDisplayName(current.get("ScoreboardObjectiveName"));
            currentObjTime = parseLong(current.get("ScoreboardObjectiveTime"));
            refreshTime = parseLong(current.get("ScoreboardObjectiveRefreshTime"));
        } else {
            HashMap<String, String> current = objectives.get(currentObjective);
            currentObjTime = parseLong(current.get("ScoreboardObjectiveTime"));
            refreshTime = parseLong(current.get("ScoreboardObjectiveRefreshTime"));
        }
        return obj;
    }
    
    private void dispose(){
        if (tim != null){
            tim.cancel();
            tim = null;
        }
        destroyAll();
    }
    
    private long parseLong(String s){
        try {
            return Long.parseLong(s);
        } catch (NumberFormatException ignored){
            return 60;
        }
    }
    
    private synchronized void destroyAll(){
        if (playerScoreboard != null || !playerScoreboard.isEmpty()){
           Map<Player, SimpleCraftScoreboard> players = new HashMap<Player, SimpleCraftScoreboard>(playerScoreboard);
           for (Map.Entry<Player, SimpleCraftScoreboard> pb : players.entrySet()){
               pb.getKey().setScoreboard(blankScoreboard);
           }
        }
    }
    
    private void loadScoreboards(){
        mainSb = new SimpleCraftScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        if (PluginConfig.ENABLE_SIDEBAR_SCOREBOARDS){
            tim.schedule(new ScoreboardTimer(), 5000, 5000);
            for (int i = 0; i < objectives.size(); i++){
                if (objectives.get(i).containsKey("top")){
                    currentObjective = i;
                    break;
                }
            }
            encodeNameAndTimes(mainSb.registerNewSimpleObjective("top"), true);
            addTop(parseTopInt(objectives.get(currentObjective).get("top")));
        }
        if (PluginConfig.USE_LVL_BELLOW_NAME){
            SimpleCraftObjective obj = mainSb.registerNewSimpleObjective("lvl");
            obj.setDisplayName(PluginConfig.LVL_NAME);
            obj.setDisplaySlot(DisplaySlot.BELOW_NAME);
        }
        if (PluginConfig.USE_RANG_BELLOW_NAME){
            UltraPvp.getRang().createRangsInScoreboard(mainSb.getBukkitScoreboard());
        }
    }
    
    public synchronized void updateLvlObjective(Player p, int lvl){
        if (mainSb.getObjective("lvl") == null)
            return;
        mainSb.getObjective("lvl").addScore(p, lvl);
    }
    
    public synchronized void updateRangObjective(Player p, String newRang){
        if (!PluginConfig.USE_RANG_BELLOW_NAME)
            return;
        Team t = mainSb.getBukkitScoreboard().getPlayerTeam(p);
        if (t != null){
            t.removePlayer(p);
        }
        mainSb.getBukkitScoreboard().getTeam(newRang).addPlayer(p);
    }
    
    public synchronized void destroy(Player p){
        playerScoreboard.remove(p);
        if (playerScoreboard == null || playerScoreboard.isEmpty()){
            playerScoreboard = new HashMap<Player, SimpleCraftScoreboard>();
        }
        p.setScoreboard(blankScoreboard);
        if (mainSb.getBukkitScoreboard().getPlayerTeam(p) != null){
            mainSb.getBukkitScoreboard().getPlayerTeam(p).removePlayer(p);
        }
        if (mainSb.getObjective("lvl") != null){
            mainSb.getObjective("lvl").resetScore(p);
        }
    }
    
    public synchronized void addPlayer(Player p, GamePlayer g){
        if (!PluginConfig.ENABLE_SCOREBOARDS)
            return;
        if (!PluginConfig.ENABLE_SIDEBAR_SCOREBOARDS)
            return;
        SimpleCraftScoreboard sb = null;
        if (objectives.get(currentObjective).containsKey("top")){
            sb = mainSb;
        } else {
            sb = mainSb.copyOfMain();
            addScores(encodeNameAndTimes(sb.registerNewSimpleObjective(currentObjective + ""), true)
                    , g);
        }
        p.setScoreboard(sb.getBukkitScoreboard());
        playerScoreboard.put(p, sb);
        updateLvlObjective(p, g.getLvL());
        updateRangObjective(p, g.getRang());
    }
    
    public SimpleCraftScoreboard getMainSb(){
        return mainSb;
    }
    
    public void cancel(boolean toggle){
        if (toggle){
            dispose();
        } else {
            cancel();
        }
    }
    
    public void cancel(){
        if (tim != null)
            tim.cancel();
    }
    
    public void addTop(int xTop){
        if (mainSb.getObjective("top") == null){
            mainSb.registerNewSimpleObjective("top");
            encodeNameAndTimes(mainSb.getObjective("top"), true);
        } else {
            encodeNameAndTimes(mainSb.getObjective("top"), false);
        }
        Map<String, Integer> top = UltraPvp.getDatabaseManager().getxTop(xTop);
        for (Map.Entry<String, Integer> gp : top.entrySet()){
            mainSb.getObjective("top").addScore(gp.getKey(), gp.getValue());
        }
        changeAllScoreboards(true);
    }
    
    private synchronized void changeAllScoreboards(boolean toggle){
        if (toggle){
            Set<Player> players = playerScoreboard.keySet();
            for (Player p : players){
                p.setScoreboard(mainSb.getBukkitScoreboard());
            }
        } else {
            Set<Player> players = playerScoreboard.keySet();
            for (Player p : players){
                if (p != null){
                    if (p.isOnline()){
                        SimpleCraftScoreboard copy = mainSb.copyOfMain();
                        addScores(encodeNameAndTimes(copy.registerNewSimpleObjective(currentObjective + ""), true)
                                , UltraPvp.getInstance().players.get(p.getName().toLowerCase()));
                        p.setScoreboard(copy.getBukkitScoreboard());
                    }
                }
            }
        }
    }
    
    private synchronized void changeSidebarObjective(){
        try {
            if (objectives.get(currentObjective).containsKey("top")){
                addTop(parseTopInt(objectives.get(currentObjective).get("top")));
            } else {
                changeAllScoreboards(false);
            }
        } catch (Exception silent){
            UltraPvp.Error(silent.getStackTrace(), silent.getMessage(), silent);
        }
        /*for (Map.Entry<String, Scoreboard> gp : playerBoards.entrySet()){
            Scoreboard board = gp.getValue();
            clearSidebar(board);
            Objective obj = encodeNameAndTimes(board);
            addScores(values, obj, UltraPvp.getInstance().players.get(gp.getKey()));
            gp.setValue(board);
            setPlayerScoreboard(gp.getKey(), board);
        }*/
    }
    
    private synchronized void addScores(SimpleCraftObjective obj, GamePlayer player){
        if (player == null){
            return;
        }
        HashMap<String, String> values = objectives.get(currentObjective);
        if (values.containsKey("top")){
          addTop(parseTopInt(values.get("top")));
        } else {
            for (Map.Entry<String, String> value : translations.entrySet()){
                if (values.containsKey(value.getValue())){
                    obj.addScore(value.getValue(), player.getFromString(value.getKey()));
                }
            }
        }
    }
    
    private int parseTopInt(String s){
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e){
            return 10;
        }
    }
    
    private synchronized void encodeSidebar(ConfigurationSection sidebar){
        if (sidebar == null)
            return;
        for (String keys : sidebar.getKeys(false)){
            HashMap<String, String> values = new HashMap<String, String>();
            values.put("ScoreboardObjectiveTime" ,sidebar.getString(keys + ".time", ""));
            values.put("ScoreboardObjectiveName" ,sidebar.getString(keys + ".name", "&bSidebar").replaceAll("&", "§"));
            values.put("ScoreboardObjectiveRefreshTime" ,sidebar.getString(keys + ".refresh", "-1"));
            for (String key : sidebar.getConfigurationSection(keys +".values").getKeys(false)){
                values.put(key.replaceAll("&", "§"), sidebar.getString(keys + ".values." + key, ""));
            }
            objectives.add(values);
            addTranslations(values);
        }
    }
    
    public synchronized void update(String how, Player player, GamePlayer gp){
        if (!PluginConfig.ENABLE_SIDEBAR_SCOREBOARDS)
            return;
        HashMap<String, String> codedSidebar = objectives.get(currentObjective);
        Iterator<Player> it = playerScoreboard.keySet().iterator();
        if (it.hasNext()){
            if (playerScoreboard.get(it.next()).equals(mainSb)){
                for (Map.Entry<String, Integer> gps : UltraPvp.getDatabaseManager().getxTop(parseTopInt(codedSidebar.get("top"))).entrySet()){
                    mainSb.getObjective("top").addScore(gps.getKey(), gps.getValue());
                }
            }
            return;
        }
        SimpleCraftScoreboard board = playerScoreboard.get(player);
        SimpleCraftObjective obj = board.getObjective(currentObjective + "");
        if (obj == null){
            return;
        }
        if (how.equalsIgnoreCase("all")){
            if (codedSidebar.containsKey("top")){
                addTop(parseTopInt(codedSidebar.get("top")));
            }
            for (Map.Entry<String, String> values : translations.entrySet()){
                if (codedSidebar.containsKey(values.getValue())){
                    obj.addScore(values.getValue(), gp.getFromString(values.getKey()));
                    /*obj.getScore(Bukkit.getOfflinePlayer(values.getValue()))
                    .setScore(gp.getFromString(values.getKey()));*/
                }
            }
        } else {
            if (codedSidebar.containsKey(translations.get(how))){
                obj.addScore(translations.get(how), gp.getFromString(how));
                /*obj.getScore(Bukkit.getOfflinePlayer(translations.get(how)))
                .setScore(gp.getFromString(how));*/
            }
        }
    }
    
    private synchronized void updateAll(){
        try {
            Map<String, GamePlayer> players = new HashMap<String, GamePlayer>(UltraPvp.getInstance().players); 
            for (Map.Entry<String, GamePlayer> gp : players.entrySet()){
                update("all", Bukkit.getPlayer(gp.getKey()), gp.getValue());
            }
        } catch (Exception silent){
            UltraPvp.Error(silent.getStackTrace(), silent.getMessage(), silent);
        }
    }
    
    private void addTranslations(HashMap<String, String> values){
        for (Map.Entry<String, String> map : values.entrySet()){
            if (map.getValue().equalsIgnoreCase("%kills%")){
                translations.put("kills", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%deaths%")){
                translations.put("deaths", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%points%")){
                translations.put("points", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%lvl%")){
                translations.put("lvl", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%money%")){
                translations.put("money", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%killstonextrang%")){
                translations.put("killstonextrang", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%position%")){
                translations.put("pos", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%kills_session%")){
                translations.put("kills_s", map.getKey());
            }
        }
    }
    
    private class ScoreboardTimer extends TimerTask {

        @Override
        public void run() {
            elapsedTime+= 5;
            if (elapsedTime >= currentObjTime && currentObjTime != -1){
                currentObjective++;
                if (currentObjective == objectives.size())
                    currentObjective = 0;
                changeSidebarObjective();
                elapsedTime = 0;
                return;
            }
            if (refreshTime != -1 && elapsedTime%refreshTime == 0){
                updateAll();
            }
        }
        
    }
    
}