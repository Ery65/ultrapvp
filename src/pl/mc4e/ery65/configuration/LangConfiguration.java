package pl.mc4e.ery65.configuration;

import java.io.File;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import pl.minecraft4ever.glowna.UltraPvp;

public class LangConfiguration {
    
    private static String ITEM_NGR;
    private static String MONEY_NGR;
    private static String NEXT_LVL;
    private static String MAX_LVL_OFF;
    private static String MAX_LVL_ON;
    private static String ITEM_ONCE_NGR;
    private static String MONEY_ONCE_NGR;
    private static String COMP_STATS;
    private static String YOUR_STATS;
    private static String OTHER_STATS;
    private static String COMP_LVL;
    private static String PLAYER_LVL;
    private static String PLAYER_OTHER;
    private static String LVL_USAGE;
    private static String SUCCES_SET_LVL;
    private static String SUCCES_ADD_LVL;
    private static String SUCCES_REM_LVL;
    private static String WRONG_LVL;
    private static String TOO_LARGE_LVL;
    private static String RESET;
    private static String RESET_OTHER;
    private static String ECON_USAGE;
    private static String ECON_VALUE;
    private static String WRONG_BALANCE;
    private static String WRONG_PAYMENT;
    private static String YOURSELF_PAYMENT;
    private static String PAYMENT_TO_PLAYER;
    private static String PAYMENT_FROM_PLAYER;
    private static String COMP_WARN_ECON;
    private static String YOUR_BALANCE;
    private static String OTHER_BALANCE;
    private static String RESET_ACCOUNT;
    private static String SUCCES_ADD;
    private static String SUCCES_REM;
    private static String SUCCES_SET;
    private static String COMP_CANT_PAY;
    private static String WRONG_VALUE;
    private static String WRONG_PLAYER;
    private static String NO_PERM;
    private static String NO_SLOT_IN_INV;
    private static String CHAT_BLOCK;
    
    private static FileConfiguration cfg;
    
    static {
        load();
    }
    
    private static void load(){
        File jezyki = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "lang");
        File file = new File(jezyki + File.separator + PluginConfig.LANG + ".yml");
        if (!file.exists()){
            jezyki.mkdirs();
            UltraPvp.copy(UltraPvp.getInstance().getResource(PluginConfig.LANG + ".yml"), file);
        }
        cfg = YamlConfiguration.loadConfiguration(file);
        
        WRONG_VALUE = cfg.getString("zla-wartosc", "&cPodana wartosc jest nieprawidlowa, musisz podac liczbe czyli np: &a10")
                .replaceAll("%,%", "\'").replaceAll("&", "§");
        WRONG_PLAYER = cfg.getString("zly-gracz", "&cPodales zly nick, lub gracza nie ma na serwerze")
                .replaceAll("%,%", "\'").replaceAll("&", "§");
        NO_PERM = cfg.getString("No-Permission", "&cNie masz uprawnien by uzywac tej komendy")
                .replaceAll("%,%", "\'").replaceAll("&", "§");
        NO_SLOT_IN_INV = cfg.getString("No-free-space-in-inventory", "&cNie masz miejsca w ekwipunku!")
                .replaceAll("%,%", "\'").replaceAll("&", "§");
        CHAT_BLOCK = cfg.getString("Chat-Disable", "&cChat jest wylaczony, dopoki nie zdobedziesz poziomu %lvl%")
                .replaceAll("%lvl%", PluginConfig.ALLOW_CHAT_LVL+"").replaceAll("&", "§");
        
        ConfigurationSection messages = cfg.getConfigurationSection("wiad-gracza");
        
        ITEM_NGR = messages.getString("przedmiot", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        MONEY_NGR = messages.getString("kasa", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        NEXT_LVL = messages.getString("przy-lvl", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        MAX_LVL_OFF = messages.getString("max-lvl-nagrody-off", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        MAX_LVL_ON = messages.getString("max-lvl-nagrody-on", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        
        ConfigurationSection once = cfg.getConfigurationSection("wiad-ngr-poz");
        
        ITEM_ONCE_NGR = once.getString("item", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        MONEY_ONCE_NGR = once.getString("kasa", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        
        ConfigurationSection stats = cfg.getConfigurationSection("Statystyki");
        
        COMP_STATS = stats.getString("konsola", "").replaceAll("%,%", "\'".replaceAll("&", "§"));
        YOUR_STATS = stats.getString("gracz", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        OTHER_STATS = stats.getString("inny-gracz", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        
        ConfigurationSection lvl = cfg.getConfigurationSection("Poziomy");
        
        COMP_LVL = lvl.getString("konsola", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        PLAYER_LVL = lvl.getString("gracz", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        PLAYER_OTHER = lvl.getString("gracz-inny", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        LVL_USAGE = lvl.getString("uzycie", "&aUzycie: &c/poziom <nick> <reset|ustaw|dodaj|odejmij> <ilosc>")
                .replaceAll("%,%", "\'").replaceAll("&", "§");
        SUCCES_SET_LVL = lvl.getString("p-u-p", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        SUCCES_ADD_LVL = lvl.getString("p-d-p", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        SUCCES_REM_LVL = lvl.getString("p-o-p", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        WRONG_LVL = lvl.getString("zlypoziom", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        TOO_LARGE_LVL = lvl.getString("zaduzy-poz", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        RESET = lvl.getString("reset", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        RESET_OTHER = lvl.getString("reset-inny", "").replaceAll("%,%", "\'").replaceAll("&", "§");
        
        ConfigurationSection econ = cfg.getConfigurationSection("Ekon");
        
        ECON_USAGE = econ.getString("uzycie","").replaceAll("%,%", "\'").replaceAll("&", "§");
        ECON_VALUE = econ.getString("wartosc","").replaceAll("%,%", "\'").replaceAll("&", "§");
        WRONG_BALANCE = econ.getString("zlybalans","").replaceAll("%,%", "\'").replaceAll("&", "§");
        WRONG_PAYMENT = econ.getString("zlyprzelew","").replaceAll("%,%", "\'").replaceAll("&", "§");
        YOURSELF_PAYMENT = econ.getString("przelewdosiebie","").replaceAll("%,%", "\'").replaceAll("&", "§");
        PAYMENT_TO_PLAYER = econ.getString("przeldg","").replaceAll("%,%", "\'").replaceAll("&", "§");
        PAYMENT_FROM_PLAYER = econ.getString("przelodg","").replaceAll("%,%", "\'").replaceAll("&", "§");
        COMP_WARN_ECON = econ.getString("komputer","").replaceAll("%,%", "\'").replaceAll("&", "§");
        YOUR_BALANCE = econ.getString("stan-twoj","").replaceAll("%,%", "\'").replaceAll("&", "§");
        OTHER_BALANCE = econ.getString("stan-innego","").replaceAll("%,%", "\'").replaceAll("&", "§");
        RESET_ACCOUNT = econ.getString("reset","").replaceAll("%,%", "\'").replaceAll("&", "§");
        SUCCES_ADD = econ.getString("dod","").replaceAll("%,%", "\'").replaceAll("&", "§");
        SUCCES_REM = econ.getString("odj","").replaceAll("%,%", "\'").replaceAll("&", "§");
        SUCCES_SET = econ.getString("pomust","").replaceAll("%,%", "\'").replaceAll("&", "§");
        COMP_CANT_PAY = econ.getString("przelewkompa","").replaceAll("%,%", "\'").replaceAll("&", "§");
    }
    
    public static String getItemNgr(){
        return new String(ITEM_NGR);
    }
    
    public static String getMoneyNgr(){
        return new String(MONEY_NGR);
    }
    
    public static String getNextLvl(){
        return new String(NEXT_LVL);
    }
    
    public static String getMaxLvLOff(){
        return new String(MAX_LVL_OFF);
    }
    
    public static String getMaxLvLOn(){
        return new String(MAX_LVL_ON);
    }
    
    public static String getItemOnceNgr(){
        return new String(ITEM_ONCE_NGR);
    }
    
    public static String getMoneyOnceNgr(){
        return new String(MONEY_ONCE_NGR);
    }
    
    public static String getCompStats(){
        return new String(COMP_STATS);
    }
    
    public static String getYourStats(){
        return new String(YOUR_STATS);
    }
    
    public static String getOtherStats(){
        return new String(OTHER_STATS);
    }
    
    public static String getCompLvL(){
        return new String(COMP_LVL);
    }
    
    public static String getPlayerLvL(){
        return new String(PLAYER_LVL);
    }
    
    public static String getOtherPlayerLvL(){
        return new String(PLAYER_OTHER);
    }
    
    public static String getLvLUsage(){
        return new String(LVL_USAGE);
    }
    
    public static String getSucccesSetLvL(){
        return new String(SUCCES_SET_LVL);
    }
    
    public static String getSuccesAdd(){
        return new String(SUCCES_ADD_LVL);
    }
    
    public static String getSuccesRem(){
        return new String(SUCCES_REM_LVL);
    }
    
    public static String getWrongLvL(){
        return new String(WRONG_LVL);
    }
    
    public static String getTooLargeLvL(){
        return new String(TOO_LARGE_LVL);
    }
    
    public static String getReset(){
        return new String(RESET);
    }
    
    public static String getResetOther(){
        return new String(RESET_OTHER);
    }
    
    public static String getEconUsage(){
        return new String(ECON_USAGE);
    }
    
    public static String getEconValue(){
        return new String(ECON_VALUE);
    }
    
    public static String getWrongBalance(){
        return new String(WRONG_BALANCE);
    }
    
    public static String getWrongPayment(){
        return new String(WRONG_PAYMENT);
    }
    
    public static String getYourselfPayment(){
        return new String(YOURSELF_PAYMENT);
    }
    
    public static String getPaymentToPlayer(){
        return new String(PAYMENT_TO_PLAYER);
    }
    
    public static String getPaymentFromPlayer(){
        return new String(PAYMENT_FROM_PLAYER);
    }
    
    public static String getCompWarnEconomy(){
        return new String(COMP_WARN_ECON);
    }
    
    public static String getYourBalance(){
        return new String(YOUR_BALANCE);
    }
    
    public static String getOtherBalance(){
        return new String(OTHER_BALANCE);
    }
    
    public static String getResetBalance(){
        return new String(RESET_ACCOUNT);
    }
    
    public static String getSuccesAddEcon(){
        return new String(SUCCES_ADD);
    }
    
    public static String getSuccesRemEcon(){
        return new String(SUCCES_REM);
    }
    
    public static String getSuccesSetEcon(){
        return new String(SUCCES_SET);
    }
    
    public static String getCompPayment(){
        return new String(COMP_CANT_PAY);
    }
    
    public static String getWrongValue(){
        return new String(WRONG_VALUE);
    }
    
    public static String getWrongPlayer(){
        return new String(WRONG_PLAYER);
    }
    
    public static String getNoPerm(){
        return new String(NO_PERM);
    }
    
    public static String getNoSlotInInventory(){
        return new String(NO_SLOT_IN_INV);
    }
    
    public static String getBlockChat(){
        return CHAT_BLOCK;
    }
    
    public static FileConfiguration getConfig(){
        return cfg;
    }
    
    public static void reload(){
        load();
    }
    
}
